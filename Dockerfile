FROM python:3.8

WORKDIR /app

COPY . /app

RUN  pip3 install --no-cache-dir -r requirements.txt \
&&pip --no-cache-dir install uwsgi==2.0.23 

#&&pip --no-cache-dir install gunicorn==20.1.0  
#&&useradd -ms /bin/bash django && chown -R django /app
#&&chmod 0775 /app/media && chown -R django /app 

EXPOSE 8080

#USER django

ENTRYPOINT [ "/bin/sh", "/app/entrypoint.sh" ]

